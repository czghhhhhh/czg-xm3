package cn.jasonone.vo;

import cn.jasonone.bean.Role;
import lombok.Data;

@Data
public class RoleVO extends Role {
	private int[] permissionIds;
}
