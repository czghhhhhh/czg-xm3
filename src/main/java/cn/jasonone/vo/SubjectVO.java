package cn.jasonone.vo;

import cn.jasonone.bean.Subject;
import cn.jasonone.bean.UserInfo;
import lombok.Data;

@Data
public class SubjectVO extends Subject {
    private UserInfo teacher;
    private UserInfo student;
}
