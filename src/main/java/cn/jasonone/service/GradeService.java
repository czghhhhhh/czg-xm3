package cn.jasonone.service;

import java.util.List;
import cn.jasonone.bean.Grade;
import cn.jasonone.bean.GradeExample;
public interface GradeService{


    long countByExample(GradeExample example);

    int deleteByExample(GradeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Grade record);

    int insertSelective(Grade record);

    List<Grade> selectByExample(GradeExample example);

    Grade selectByPrimaryKey(Integer id);

    int updateByExampleSelective(Grade record,GradeExample example);

    int updateByExample(Grade record,GradeExample example);

    int updateByPrimaryKeySelective(Grade record);

    int updateByPrimaryKey(Grade record);

}
