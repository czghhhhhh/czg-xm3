package cn.jasonone.service;

import cn.jasonone.bean.TestTopicMapping;
import cn.jasonone.bean.TestTopicMappingExample;
import java.util.List;
public interface TestTopicMappingService{


    long countByExample(TestTopicMappingExample example);

    int deleteByExample(TestTopicMappingExample example);

    int deleteByPrimaryKey(Integer testId,Integer topicId);

    int insert(TestTopicMapping record);

    int insertSelective(TestTopicMapping record);

    List<TestTopicMapping> selectByExample(TestTopicMappingExample example);

    int updateByExampleSelective(TestTopicMapping record,TestTopicMappingExample example);

    int updateByExample(TestTopicMapping record,TestTopicMappingExample example);

}
