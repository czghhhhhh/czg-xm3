package cn.jasonone.service;

import cn.jasonone.bean.SubjectExample;
import java.util.List;
import cn.jasonone.bean.Subject;
import cn.jasonone.vo.SubjectVO;

public interface SubjectService{


    long countByExample(SubjectExample example);

    int deleteByExample(SubjectExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Subject record);

    int insertSelective(Subject record);

    List<SubjectVO> selectByExample(SubjectExample example);

    Subject selectByPrimaryKey(Integer id);

    int updateByExampleSelective(Subject record,SubjectExample example);

    int updateByExample(Subject record,SubjectExample example);

    int updateByPrimaryKeySelective(Subject record);

    int updateByPrimaryKey(Subject record);

}
