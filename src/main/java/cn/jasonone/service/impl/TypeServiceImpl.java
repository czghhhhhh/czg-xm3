package cn.jasonone.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import cn.jasonone.bean.TypeExample;
import java.util.List;
import cn.jasonone.bean.Type;
import cn.jasonone.mapper.TypeMapper;
import cn.jasonone.service.TypeService;
@Service
public class TypeServiceImpl implements TypeService{

    @Resource
    private TypeMapper typeMapper;

    @Override
    public long countByExample(TypeExample example) {
        return typeMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(TypeExample example) {
        return typeMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return typeMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Type record) {
        return typeMapper.insert(record);
    }

    @Override
    public int insertSelective(Type record) {
        return typeMapper.insertSelective(record);
    }

    @Override
    public List<Type> selectByExample(TypeExample example) {
        return typeMapper.selectByExample(example);
    }

    @Override
    public Type selectByPrimaryKey(Integer id) {
        return typeMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByExampleSelective(Type record,TypeExample example) {
        return typeMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(Type record,TypeExample example) {
        return typeMapper.updateByExample(record,example);
    }

    @Override
    public int updateByPrimaryKeySelective(Type record) {
        return typeMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Type record) {
        return typeMapper.updateByPrimaryKey(record);
    }

}
