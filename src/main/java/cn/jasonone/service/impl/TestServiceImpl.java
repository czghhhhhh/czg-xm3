package cn.jasonone.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.jasonone.bean.Test;
import cn.jasonone.bean.TestExample;
import cn.jasonone.mapper.TestMapper;
import cn.jasonone.service.TestService;
@Service
public class TestServiceImpl implements TestService{

    @Resource
    private TestMapper testMapper;

    @Override
    public long countByExample(TestExample example) {
        return testMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(TestExample example) {
        return testMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return testMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Test record) {
        return testMapper.insert(record);
    }

    @Override
    public int insertSelective(Test record) {
        return testMapper.insertSelective(record);
    }

    @Override
    public List<Test> selectByExample(TestExample example) {
        return testMapper.selectByExample(example);
    }

    @Override
    public Test selectByPrimaryKey(Integer id) {
        return testMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByExampleSelective(Test record,TestExample example) {
        return testMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(Test record,TestExample example) {
        return testMapper.updateByExample(record,example);
    }

    @Override
    public int updateByPrimaryKeySelective(Test record) {
        return testMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Test record) {
        return testMapper.updateByPrimaryKey(record);
    }

}
