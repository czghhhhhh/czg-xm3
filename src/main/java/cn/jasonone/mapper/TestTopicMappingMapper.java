package cn.jasonone.mapper;

import cn.jasonone.bean.TestTopicMapping;
import cn.jasonone.bean.TestTopicMappingExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface TestTopicMappingMapper {
    long countByExample(TestTopicMappingExample example);

    int deleteByExample(TestTopicMappingExample example);

    int deleteByPrimaryKey(@Param("testId") Integer testId, @Param("topicId") Integer topicId);

    int insert(TestTopicMapping record);

    int insertSelective(TestTopicMapping record);

    List<TestTopicMapping> selectByExample(TestTopicMappingExample example);

    int updateByExampleSelective(@Param("record") TestTopicMapping record, @Param("example") TestTopicMappingExample example);

    int updateByExample(@Param("record") TestTopicMapping record, @Param("example") TestTopicMappingExample example);
}