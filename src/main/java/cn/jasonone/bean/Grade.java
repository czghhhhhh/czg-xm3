package cn.jasonone.bean;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Grade implements Serializable {
    /**
    * 成绩id
    */
    private Integer id;

    /**
    * 分数
    */
    private Double grade;

    /**
    * 外键、学生学号
    */
    private Integer sId;

    /**
    * 外键、科目id
    */
    private Integer subId;

    /**
    * 外键、试卷id
    */
    private Integer testId;

    /**
    * 所获得绩点（通过分数是否及格判断）
    */
    private Integer point;

    /**
    * 是否补考
    */
    private Integer makeup;

    private static final long serialVersionUID = 1L;
}