package cn.jasonone.bean;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Topic implements Serializable {
    /**
    * 主键、题目id
    */
    private Integer id;

    /**
    * 外键、题目类型
    */
    private Integer typeId;

    /**
    * 题目内容
    */
    private String content;

    /**
    * 外键、所属哪个科目下的题目
    */
    private Integer subId;

    private static final long serialVersionUID = 1L;
}