package cn.jasonone.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo implements Serializable {
    private Integer id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最后更新时间
     */
    private Date updateTime;

    /**
     * 电话号码
     */
    private String phone;

    /**
     * 学分
     */
    private Integer credit;

    /**
     * 真实姓名
     */
    private String truename;

    private List<Role> roles;

    private static final long serialVersionUID = 1L;
}