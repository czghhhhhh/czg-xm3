package cn.jasonone.bean;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Test implements Serializable {
    /**
    * 试卷id
    */
    private Integer id;

    /**
    * 试卷名称
    */
    private String name;

    /**
    * 考试总时长
    */
    private Integer time;

    /**
    * 考试时间
    */
    private String date;

    /**
    * 考试分数
    */
    private Integer grade;

    /**
    * 所属科目、外键
    */
    private Integer subId;

    /**
    * 用户id（角色为学生）
    */
    private Integer userSid;

    /**
    * 用户id（角色为老师）
    */
    private Integer userTid;

    private static final long serialVersionUID = 1L;
}