package cn.jasonone.bean;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Subject implements Serializable {
    /**
    * 科目id
    */
    private Integer id;

    /**
    * 科目名称
    */
    private String name;

    /**
    * 科目绩点（学分）
    */
    private Integer point;

    /**
    * 用户id（role为老师）
    */
    private Integer userId;

    private static final long serialVersionUID = 1L;
}