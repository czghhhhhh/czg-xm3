package cn.jasonone.bean;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestTopicMapping implements Serializable {
    /**
    * 试卷id主键
    */
    private Integer testId;

    /**
    * 题目id主键
    */
    private Integer topicId;

    private static final long serialVersionUID = 1L;
}