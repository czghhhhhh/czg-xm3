package cn.jasonone.controller;

import cn.jasonone.annotations.Authorization;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/teacher")
public class TeacherController {
    private String viewRoot() {
        return "teacherManager/";
    }

    @Authorization(role = {"TEACHER"})
    @GetMapping(produces = "text/html")
    public ModelAndView index() {
        return new ModelAndView(viewRoot() + "index");
    }
}
