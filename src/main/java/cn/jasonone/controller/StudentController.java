package cn.jasonone.controller;

import cn.jasonone.annotations.Authorization;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/student")
public class StudentController {
    private String viewRoot() {
        return "studentManager/";
    }

    @Authorization(role = {"STUDENT"})
    @GetMapping(produces = "text/html")
    public ModelAndView index() {
        return new ModelAndView(viewRoot() + "index");
    }
}
