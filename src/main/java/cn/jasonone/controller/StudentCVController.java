package cn.jasonone.controller;

import cn.jasonone.annotations.Authorization;
import cn.jasonone.bean.Subject;
import cn.jasonone.bean.SubjectExample;
import cn.jasonone.bean.Test;
import cn.jasonone.bean.TestExample;
import cn.jasonone.service.SubjectService;
import cn.jasonone.service.TestService;
import cn.jasonone.service.UserInfoService;
import cn.jasonone.vo.HttpResult;
import cn.jasonone.vo.SubjectVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/student/cv")
public class StudentCVController {
    @Resource(name = "userInfoService")
    private UserInfoService baseService;
    @Resource
    private SubjectService subjectService;
    @Resource
    private TestService testService;

    private String viewRoot(){
        return "studentManager/";
    }

    @Authorization(role = {"STUDENT"})
    @GetMapping(produces = "text/html")
    public ModelAndView index(){
        return new ModelAndView(viewRoot()+"curricula-variable");
    }

    @GetMapping(value = "/{userId}",produces = "application/json")
    public HttpResult<Subject> findAll(
            @RequestParam(defaultValue = "1")int page,
            @RequestParam(defaultValue = "10")int limit,
            SubjectVO subject,
            @PathVariable Integer userId
    ) {
        PageHelper.startPage(page, limit);

        SubjectExample example = new SubjectExample();
        SubjectExample.Criteria criteria = example.createCriteria();

        if(subject != null ){
            if(subject.getName() != null && !subject.getName().isEmpty()){
                criteria.andNameLike("%"+subject.getName()+"%");
            }
        }

        List<SubjectVO> subjects = subjectService.selectByExample(example);

        for (SubjectVO vo : subjects) {
            TestExample testExample = new TestExample();
            TestExample.Criteria testCriteria = testExample.createCriteria();
            testCriteria.andSubIdEqualTo(vo.getId()).andUserSidEqualTo(userId);
            int count = this.testService.selectByExample(testExample).size();
            if (count == 1) {
                vo.setStudent(this.baseService.selectByPrimaryKey(userId));
            }
        }
        PageInfo<SubjectVO> pageInfo = new PageInfo<>(subjects);
        HttpResult rs = new HttpResult();
        rs.setData(subjects);
        rs.setCount(pageInfo.getTotal());
        return rs;
    }


    @PutMapping(value = "/insert/{userId}")
    public HttpResult insert(@RequestBody SubjectVO subject,@PathVariable Integer userId){
        System.out.println("进入insert");
        System.out.println(subject.getId());
        try {
            TestExample example = new TestExample();
            TestExample.Criteria criteria1 = example.createCriteria();
            criteria1.andUserSidEqualTo(userId).andSubIdEqualTo(subject.getId());
            List<Test> tests = this.testService.selectByExample(example);
            if (tests.size()!=0) {
                return new HttpResult(20002,"该门课程您已选过，请不要重复选择");
            }
            example = new TestExample();
            TestExample.Criteria criteria2 = example.createCriteria();
            criteria2.andSubIdEqualTo(subject.getId()).andUserSidEqualTo(-1);
            Test test = this.testService.selectByExample(example).get(0);
            test.setUserSid(userId);
            test.setId(null);

            this.testService.insertSelective(test);
            return HttpResult.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return new HttpResult(20001, "课程新增失败:"+e.getMessage());
        }

    }
    @PutMapping(value = "/inserts/{userId}")
    public HttpResult inserts(@PathVariable Integer userId,Integer[] ids){
        System.out.println("进入inserts");

        try {
            TestExample example = new TestExample();
            TestExample.Criteria criteria1 = example.createCriteria();
            criteria1.andUserSidEqualTo(userId);
            criteria1.andSubIdIn(Arrays.asList(ids));
            List<Test> tests = this.testService.selectByExample(example);
            if (tests.size()!=0) {
                return new HttpResult(20004,"您已选择的选课可能包括您现在选的一部分");
            }
            example = new TestExample();
            TestExample.Criteria criteria2 = example.createCriteria();
            criteria2.andSubIdIn(Arrays.asList(ids)).andUserSidEqualTo(-1);
            List<Test> initTests = this.testService.selectByExample(example);
            for (Test test : initTests) {
                test.setUserSid(userId);
                test.setId(null);
                this.testService.insertSelective(test);
            }
            return HttpResult.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return new HttpResult(20001, "课程新增失败:"+e.getMessage());
        }

    }
    @DeleteMapping("/delete/{userId}")
    public HttpResult delete(@RequestBody SubjectVO subject,@PathVariable Integer userId){
        System.out.println("进入delete");

        try {
            TestExample example = new TestExample();
            TestExample.Criteria criteria1 = example.createCriteria();
            criteria1.andUserSidEqualTo(userId).andSubIdEqualTo(subject.getId());
            List<Test> tests = this.testService.selectByExample(example);
            if(tests.size() == 0){
                return new HttpResult(20002,"该课程您还未选,无法删除");
            }
            this.testService.deleteByExample(example);
            return HttpResult.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return new HttpResult(20001, "课程删除失败:"+e.getMessage());
        }

    }
    @DeleteMapping("/deletes/{userId}")
    public HttpResult deletes(@PathVariable Integer userId,Integer[] ids){
        System.out.println("进入deletes");

        try {
            TestExample example = new TestExample();
            TestExample.Criteria criteria1 = example.createCriteria();
            criteria1.andUserSidEqualTo(userId).andSubIdIn(Arrays.asList(ids));
            List<Test> tests = this.testService.selectByExample(example);
            if(tests.size() != ids.length){
                return new HttpResult(20002,"现在选中的课程中可能有一部分您还未选过,无法删除");
            }

            this.testService.deleteByExample(example);
            return HttpResult.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return new HttpResult(20001, "课程删除失败:"+e.getMessage());
        }

    }
}
