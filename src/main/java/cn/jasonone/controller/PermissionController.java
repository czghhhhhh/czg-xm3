package cn.jasonone.controller;

import cn.jasonone.bean.Permission;
import cn.jasonone.bean.PermissionExample;
import cn.jasonone.service.PermissionService;
import cn.jasonone.service.PermissionService;
import cn.jasonone.vo.HttpResult;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

//@Controller
//@ResponseBody
@RestController
@RequestMapping("/permission")
public class PermissionController {
	@Resource(name = "permissionService")
	private PermissionService baseService;
	
	private String viewRoot() {
		return "permissionManager/";
	}
	
	@GetMapping(produces = "text/html")
	public ModelAndView index() {
		return new ModelAndView(viewRoot() + "index");
	}
	
	@GetMapping(path = "/insert", produces = "text/html")
	public ModelAndView insert() {
		return new ModelAndView(viewRoot() + "insert");
	}
	
	@GetMapping(path = "/insert/{pid}", produces = "text/html")
	public ModelAndView insert(@PathVariable int pid) {
		ModelAndView mv = new ModelAndView(viewRoot() + "insert");
		mv.addObject("pid", pid);
		return mv;
	}
	
	@GetMapping(produces = "application/json")
	public HttpResult<Permission> findAll(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "10") int limit,
			Permission permission
	) {
		PageHelper.startPage(page, limit);
		
		PermissionExample example = new PermissionExample();
		// 创建条件
		PermissionExample.Criteria criteria = example.createCriteria();
		
		// where (username = ? and password =? ) or ( ... )
		if (permission != null) {
			if (permission.getName() != null && !permission.getName().isEmpty()) {
				criteria.andNameLike("%" + permission.getName() + "%");
			}
			if (permission.getCode() != null && !permission.getCode().isEmpty()) {
				criteria.andCodeLike("%" + permission.getCode() + "%");
			}
			if (permission.getStatus() != null) {
				criteria.andStatusEqualTo(permission.getStatus());
			}
		}
		
		List<Permission> permissions = baseService.selectByExample(example);
		PageInfo<Permission> pageInfo = new PageInfo<>(permissions);
		HttpResult rs = new HttpResult();
		rs.setData(permissions);
		rs.setCount(pageInfo.getTotal());
		return rs;
	}
	
	@GetMapping("/parent")
	public HttpResult<Permission> findParent(){
		PermissionExample example = new PermissionExample();
		example.createCriteria().andPidEqualTo(-1);
		List<Permission> permissions = this.baseService.selectByExample(example);
		return new HttpResult<>(0, "查询成功",permissions);
	}
	
	@PutMapping("/insert")
	public HttpResult insert(@RequestBody Permission permission) {
		try {
			if (this.baseService.insertSelective(permission) > 0) {
				return HttpResult.SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new HttpResult(40001, "权限新增失败");
	}
	
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@DeleteMapping("/delete")
	public HttpResult delete(Integer[] ids) {
		PermissionExample example;
		{
			example = new PermissionExample();
			PermissionExample.Criteria criteria = example.createCriteria();
			criteria.andPidIn(Arrays.asList(ids));
			if (this.baseService.countByExample(example) > 0) {
				return new HttpResult(4004, "删除失败: 该权限拥有子权限,请先删除子权限");
			}
		}
		
		try {
			example = new PermissionExample();
			example.createCriteria().andIdIn(Arrays.asList(ids));
			if (this.baseService.deleteByExample(example) > 0) {
				return HttpResult.SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new HttpResult(40002, "删除失败:" + e.getMessage());
		}
		return new HttpResult(40002, "删除失败");
	}
	
	@DeleteMapping("/delete/{id}")
	public HttpResult delete(@PathVariable int id) {
		PermissionExample example = new PermissionExample();
		PermissionExample.Criteria criteria = example.createCriteria();
		criteria.andPidEqualTo(id);
		if (this.baseService.countByExample(example) > 0) {
			return new HttpResult(4004, "删除失败: 该权限拥有子权限,请先删除子权限");
		}
		try {
			int row = this.baseService.deleteByPrimaryKey(id);
			if (row > 0) {
				return HttpResult.SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new HttpResult(40002, "删除失败:" + e.getMessage());
		}
		return new HttpResult(40002, "删除失败");
	}
	
	@GetMapping("/update/{id}")
	public ModelAndView update(@PathVariable int id) {
		ModelAndView mv = new ModelAndView(viewRoot() + "update");
		mv.addObject("entity", this.baseService.selectByPrimaryKey(id));
		return mv;
	}
	
	@PostMapping("/update")
	public HttpResult update(@RequestBody Permission permission) {
		try {
			if (this.baseService.updateByPrimaryKeySelective(permission) > 0) {
				return HttpResult.SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new HttpResult(40003, "修改失败:" + e.getMessage());
		}
		return new HttpResult(40003, "修改失败");
	}
}
