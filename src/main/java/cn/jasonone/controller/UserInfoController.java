package cn.jasonone.controller;

import cn.jasonone.annotations.Authorization;
import cn.jasonone.bean.Role;
import cn.jasonone.bean.UserInfo;
import cn.jasonone.bean.UserInfoExample;
import cn.jasonone.service.UserInfoService;
import cn.jasonone.vo.HttpResult;
import cn.jasonone.vo.UserInfoVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

//@Controller
//@ResponseBody
@RestController
@RequestMapping("/user")
public class UserInfoController {
	@Resource(name = "userInfoService")
	private UserInfoService baseService;
	
	private String viewRoot(){
		return "userManager/";
	}
	@Authorization(role = {"USER","USER_1","ADMINISTRATOR"})
	@GetMapping(produces = "text/html")
	public ModelAndView index(){
		return new ModelAndView(viewRoot()+"index");
	}
	@Authorization(role = {"USER","ADMINISTRATOR"},value = "USER_ADD")
	@GetMapping(path = "/insert",produces = "text/html")
	public ModelAndView insert(){
		return new ModelAndView(viewRoot()+"insert");
	}
	@Authorization(role = {"USER","USER_1","ADMINISTRATOR"})
	@GetMapping(produces = "application/json")
	public HttpResult<UserInfo> findAll(
		@RequestParam(defaultValue = "1")int page,
		@RequestParam(defaultValue = "10")int limit,
		UserInfo userInfo
	){
		PageHelper.startPage(page, limit);
		
		UserInfoExample example = new UserInfoExample();
		// 创建条件
		UserInfoExample.Criteria criteria = example.createCriteria();
		
		// where (username = ? and password =? ) or ( ... )
		if(userInfo != null ){
			if(userInfo.getUsername() != null && !userInfo.getUsername().isEmpty()){
				criteria.andUsernameLike("%"+userInfo.getUsername()+"%");
			}
			if(userInfo.getStatus() != null){
				criteria.andStatusEqualTo(userInfo.getStatus());
			}
		}
		
		List<UserInfo> userInfos = baseService.selectByExample(example);
		PageInfo<UserInfo> pageInfo = new PageInfo<>(userInfos);
		HttpResult rs = new HttpResult();
		rs.setData(userInfos);
		rs.setCount(pageInfo.getTotal());
		return rs;
	}
	@Authorization(role = {"USER","ADMINISTRATOR"},value = "USER_ADD")
	@PutMapping("/insert")
	public HttpResult insert(@RequestBody UserInfoVO userInfo){
		try {
			if(userInfo.getPassword() !=null && !userInfo.getPassword().isEmpty()){
				String pwd = DigestUtils.md5DigestAsHex(userInfo.getPassword().getBytes());
				userInfo.setPassword(pwd);
			}
			this.baseService.insertSelective(userInfo);
			return HttpResult.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return new HttpResult(20001, "用户新增失败:"+e.getMessage());
		}
		
	}
	
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@DeleteMapping("/delete")
	@Authorization(role = {"USER","ADMINISTRATOR"},value = "USER_DEL")
	public HttpResult delete(Integer[] ids){
		UserInfoExample example = new UserInfoExample();
		example.createCriteria().andIdIn(Arrays.asList(ids));
		try {
			if (this.baseService.deleteByExample(example)>0) {
				return HttpResult.SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new HttpResult(20002, "删除失败:"+e.getMessage());
		}
		return new HttpResult(20002, "删除失败");
	}
	@Authorization(role = {"USER","ADMINISTRATOR"},value = "USER_DEL")
	@DeleteMapping("/delete/{id}")
	public HttpResult delete(@PathVariable int id){
		try {
			int row = this.baseService.deleteByPrimaryKey(id);
			if(row >0){
				return HttpResult.SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new HttpResult(20002, "删除失败:"+e.getMessage());
		}
		return new HttpResult(20002, "删除失败");
	}

	@GetMapping("/update/{id}")
	public ModelAndView update(@PathVariable int id){
		ModelAndView mv = new ModelAndView(viewRoot()+"update");
		UserInfo userInfo = this.baseService.selectByPrimaryKey(id);
		mv.addObject("entity", userInfo);
		List<Role> roles = userInfo.getRoles();
		mv.addObject("roleIds", roles.stream().map(role->role.getId()+"").collect(Collectors.joining(",")));
		return mv;
	}
	@Authorization(role = {"USER","ADMINISTRATOR"},value = "USER_UPDATE")
	@PostMapping("/update")
	public HttpResult update(@RequestBody UserInfoVO userInfo){
		try {
			if(userInfo.getPassword() !=null && !userInfo.getPassword().isEmpty()){
				String pwd = DigestUtils.md5DigestAsHex(userInfo.getPassword().getBytes());
				userInfo.setPassword(pwd);
			}else{
				userInfo.setPassword(null);
			}
			this.baseService.updateByPrimaryKeySelective(userInfo);
			return HttpResult.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return new HttpResult(20003, "修改失败:"+e.getMessage());
		}
	}
}
